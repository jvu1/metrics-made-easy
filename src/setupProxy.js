const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = app => {
  app.use(
    '/api/atlassian/*',
    createProxyMiddleware({
      target: "https://chegg.atlassian.net",
      pathRewrite: {
        '^/api/atlassian': '', // rewrite path
      },
      changeOrigin: true,
    })
  );
};
