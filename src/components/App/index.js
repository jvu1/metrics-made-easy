
import SprintMetrics from '../SprintMetrics';
import NewBugs from '../NewBugs';
import ProdBugs from '../ProdBugs';
import './index.css';

function App() {
  return (
    <div className="App">
      <h1>Metrics made easy</h1>

      <SprintMetrics />
      <NewBugs />
      <ProdBugs />
    </div>
  );
}

export default App;
