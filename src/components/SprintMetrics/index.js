import { useEffect, useState } from 'react';
import './index.css';

function SprintMetrics() {  
  const [loading, setLoading] = useState(false);
  const [currentData, setCurrentData] = useState(null);
  const [lastData, setLastData] = useState(null);

  useEffect(() => {
    const rapidViewId = process.env.REACT_APP_ATLASSIAN_JIRA_RAPID_VIEW_ID;
    const authorization = `Basic ${btoa(`${process.env.REACT_APP_ATLASSIAN_BASIC_AUTH_USER}:${process.env.REACT_APP_ATLASSIAN_BASIC_AUTH_TOKEN}`)}`;

    const getAllData = async () => {
      // Get Sprints
      setLoading(true);

      const sprintRes = await fetch(`/api/atlassian/rest/greenhopper/latest/sprintquery/${rapidViewId}`, {
        headers: {
          Accept: 'application/json',
          Authorization: authorization,
        }
      }).then(res => res.json());

      console.log('sprintRes.sprints', sprintRes.sprints)

      const activeSprint = sprintRes.sprints.find(issue => issue.state.toLowerCase() === 'active');
      // const activeSprint = sprintRes.sprints.find(issue => issue.id === 9298);
      const beforeActiveSprint = sprintRes.sprints[sprintRes.sprints.indexOf(activeSprint) - 1];

      const currentRes = await fetch(`/api/atlassian/rest/greenhopper/latest/rapid/charts/sprintreport?rapidViewId=${rapidViewId}&sprintId=${activeSprint.id}`, {
        headers: {
          Accept: 'application/json',
          Authorization: authorization,
        }
      }).then(res => res.json())

      const lastRes = await fetch(`/api/atlassian/rest/greenhopper/latest/rapid/charts/sprintreport?rapidViewId=${rapidViewId}&sprintId=${beforeActiveSprint.id}`, {
        headers: {
          Accept: 'application/json',
          Authorization: authorization,
        }
      }).then(res => res.json())

      setCurrentData(currentRes);
      setLastData(lastRes);
      setLoading(false);
    };

    if (!currentData && !loading) {
      getAllData();
    }    
  }, [currentData, loading]);

  if (currentData && lastData && !loading) {
    console.log('data', currentData);

    // Sprint information
    const sprintName = currentData?.sprint?.name;
    const sprintStartDate = currentData?.sprint?.isoStartDate;
    const sprintEndDate = currentData?.sprint?.isoEndDate;

    // Committed vs uncommited / complete vs incomplete
    const completedIssues = currentData?.contents?.completedIssues;
    const incompletedIssues = currentData?.contents?.issuesNotCompletedInCurrentSprint;
    const issuesAdded = currentData?.contents?.issueKeysAddedDuringSprint;

    const lastCompletedIssues = lastData?.contents?.completedIssues;
    const lastIssuesAdded = currentData?.contents?.issueKeysAddedDuringSprint;
    const lastIncompletedIssues = lastData?.contents?.issuesNotCompletedInCurrentSprint;

    // -- filtered results
    const committedCompletedIssues = completedIssues.filter(issue => !issuesAdded[issue.key]);
    const committedIncompletedIssues = incompletedIssues.filter(issue => !issuesAdded[issue.key]);
    const lastCommittedCompletedIssues = lastCompletedIssues.filter(issue => !lastIssuesAdded[issue.key]);
    const lastCommittedIncompletedIssues = lastIncompletedIssues.filter(issue => !lastIssuesAdded[issue.key]);

    // -- counts
    const committedCompletedIssuesCount = committedCompletedIssues.length || 0;
    const committedIncompletedIssuesCount = committedIncompletedIssues.length || 0;
    const uncommitedCount = Object.keys(issuesAdded).length || 0;
    const committedCount = committedCompletedIssuesCount + committedIncompletedIssuesCount;

    // -- story points
    const committedCompletedPoints = committedCompletedIssues.reduce((mem, issue) => {
      return mem + issue?.estimateStatistic?.statFieldValue?.value || 0;
    }, 0);
    const committedIncompletedPoints = committedIncompletedIssues.reduce((mem, issue) => {
      return mem + issue?.estimateStatistic?.statFieldValue?.value || 0;
    }, 0);
    const committedTotalPoints = committedCompletedPoints + committedIncompletedPoints;
  
    const lastCommittedCompletedPoints = lastCommittedCompletedIssues.reduce((mem, issue) => {
      return mem + issue?.estimateStatistic?.statFieldValue?.value || 0;
    }, 0);
    const lastCommittedIncompletedPoints = lastCommittedIncompletedIssues.reduce((mem, issue) => {
      return mem + issue?.estimateStatistic?.statFieldValue?.value || 0;
    }, 0);
    const lastCommittedTotalPoints = lastCommittedCompletedPoints + lastCommittedIncompletedPoints;

    // Types: bugs/stories/tech stories
    // -- filtered results
    const completedBugs = completedIssues.filter(issue => issue.typeName.toLowerCase() === 'bug');
    const completedStories = completedIssues.filter(issue => issue.typeName.toLowerCase() === 'story');
    const completedTechStories = completedIssues.filter(issue => issue.typeName.toLowerCase() === 'tech story');

    // -- counts
    const completedBugsCount = completedBugs.length || 0;
    const completedStoriesCount = completedStories.length || 0;

    // -- story points
    const completedStoriesPoints = completedStories.reduce((mem, issue) => {
      return mem + issue?.estimateStatistic?.statFieldValue?.value || 0;
    }, 0);
    const completedTechStoriesPoints = completedTechStories.reduce((mem, issue) => {
      return mem + issue?.estimateStatistic?.statFieldValue?.value || 0;
    }, 0);
    const completedTotalStoryPoints = completedStoriesPoints + completedTechStoriesPoints;

    return (
      <section>
        <h2>Sprint Metrics</h2>

        <h3>{sprintName}</h3>
        <h4>{sprintStartDate} - {sprintEndDate}</h4>

        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Results</th>
              <th>Notes</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Commit vs Delivered %</td>
              <td>{(committedCompletedPoints / committedTotalPoints * 100).toFixed(0)}% ({(lastCommittedCompletedPoints / lastCommittedTotalPoints * 100).toFixed(0)}% last sprint)</td>
              <td>committedCompletedPoints / committedTotalPoints * 100</td>
            </tr>
            <tr>
              <td>Velocity change %</td>
              <td>{((committedCompletedPoints / committedTotalPoints * 100).toFixed(0) - (lastCommittedCompletedPoints / lastCommittedTotalPoints * 100).toFixed(0))}%</td>
              <td>(committedCompletedPoints / committedTotalPoints * 100) - (lastCommittedCompletedPoints / lastCommittedTotalPoints * 100)</td>
            </tr>
            <tr>
              <td>Biz vs Tech</td>
              <td>{completedStoriesPoints / completedTotalStoryPoints * 100}:{completedTechStoriesPoints / completedTotalStoryPoints * 100}</td>
              <td>completedStoriesPoints / completedTotalStoryPoints * 100 : completedTechStoriesPoints / completedTotalStoryPoints * 100</td>
            </tr>
            <tr>
              <td>Bug vs Stories</td>
              <td>{completedBugsCount}:{completedStoriesCount}</td>
              <td>completedBugsCount : completedNonBugsCount</td>
            </tr>
            <tr>
              <td>Planned Stories vs Unplanned</td>
              <td>{committedCount}:{uncommitedCount}</td>
              <td>committedCount : uncommitedCount</td>
            </tr>
          </tbody>
        </table>
      </section>
    );
  }

  return 'Loading data...'
}

export default SprintMetrics;


