import { useEffect, useState } from 'react';
import './index.css';

const fetchIssues = (authorization, priority, createdLessThatDays) => {
  return fetch(`/api/atlassian/rest/api/2/search?jql=issuetype%20%3D%20Bug%20AND%20priority%20%3D%20${priority}%20AND%20status%20not%20in%20(Closed%2C%20Done%2C%20Resolved%2C%20"Will%20Not%20Complete"%2C%20"Ready%20for%20Deploy"%2C%20"Ready%20for%20Prod"%2C%20"Ready%20for%20Stage"%2C%20"in%20test"%2C%20"in%20testing"%2C%20"Ready%20for%20Test")%20AND%20createdDate%20>%20${createdLessThatDays}%20AND%20project%20%3D%20SM%20ORDER%20BY%20created%20DESC%2C%20status%20DESC`, {
    headers: {
      Accept: 'application/json',
      Authorization: authorization,
    }
  }).then(res => res.json())
};

function NewBugs() {  
  const [loading, setLoading] = useState(false);
  const [currentData, setCurrentData] = useState(null);


  useEffect(() => {
    // const sprintId = '9298';
    // const rapidViewId = '1381';
    const authorization = `Basic ${btoa(`${process.env.REACT_APP_ATLASSIAN_BASIC_AUTH_USER}:${process.env.REACT_APP_ATLASSIAN_BASIC_AUTH_TOKEN}`)}`;

    const getAllData = async () => {
      // Get Sprints
      setLoading(true);

      const p0bugsRes = await fetchIssues(authorization, 'P0', '-14d');
      const p1bugsRes = await fetchIssues(authorization, 'P1', '-14d');
      const p2bugsRes = await fetchIssues(authorization, 'P2', '-14d');
      const p3bugsRes = await fetchIssues(authorization, 'P3', '-14d');

      setCurrentData({
        p0: p0bugsRes,
        p1: p1bugsRes,
        p2: p2bugsRes,
        p3: p3bugsRes
      });
      setLoading(false);
    };

    if (!currentData && !loading) {
      getAllData();
    }    
  }, [currentData, loading]);

  if (currentData && !loading) {
    console.log('data', currentData);

    return (
      <section>
        <h2>New bugs</h2>

        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Results</th>
              <th>Notes</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>P0 - 24hrs</td>
              <td>{currentData.p0.total}</td>
              <td>{currentData.p0.issues.map(issue => <> <a href={`https://chegg.atlassian.net/browse/${issue.key}`} target="_blank">{issue.key}</a> </>)}</td>
            </tr>
            <tr>
              <td>P1 - 3weeks</td>
              <td>{currentData.p1.total}</td>
              <td>{currentData.p1.issues.map(issue => <> <a href={`https://chegg.atlassian.net/browse/${issue.key}`} target="_blank">{issue.key}</a> </>)}</td>
            </tr>
            <tr>
              <td>P2 - 8weeks</td>
              <td>{currentData.p2.total}</td>
              <td>{currentData.p2.issues.map(issue => <> <a href={`https://chegg.atlassian.net/browse/${issue.key}`} target="_blank">{issue.key}</a> </>)}</td>
            </tr>
            <tr>
              <td>P3 - 6months</td>
              <td>{currentData.p3.total}</td>
              <td>{currentData.p3.issues.map(issue => <> <a href={`https://chegg.atlassian.net/browse/${issue.key}`} target="_blank">{issue.key}</a> </>)}</td>
            </tr>
          </tbody>
        </table>
      </section>
    );
  }

  return 'Loading data...'
}

export default NewBugs;
