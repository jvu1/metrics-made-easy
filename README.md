# Metrics Made Easy


## Setting up 
* 1 JIRA account
* 1 setup API Token

## Setting up API Token
To setup your API token first 
1. Log into JIRA
1. Make note of your "rapidView" ID by going to your sprint board and finding it in your URL bar (it should be something like this: `https://chegg.atlassian.net/secure/RapidBoard.jspa?rapidView=1381`)
1. Create an API Token by going to: https://id.atlassian.com/manage/api-tokens.
   * use any label you want.  i.e. metrics-made-easy
   * copy that and hold onto it
1. Create `.env.local` file in the root directory
1. Add in the following
```
REACT_APP_ATLASSIAN_JIRA_RAPID_VIEW_ID=[your rapid view ID you found before]
REACT_APP_ATLASSIAN_BASIC_AUTH_USER=[you login here]
REACT_APP_ATLASSIAN_BASIC_AUTH_TOKEN=[the API you just made before]
```
1. Save it
1. Run the app
## Running the app
Make sure you "yarn" to install the appropriate libraries.  Then...
#### `yarn start`
You must run this app in SSL. To start use the following

Go to `https://localhost:3000` and see your site.
